$(document).ready(function(){
    'use strict'
    
    ////////////////////////////// Shown Dropdown Submenu Onclick //////////////// 
    $(".dropdown").click(function(){
        $(".dropdown-menu").toggleClass("dropdown-menu--active");
    });

    ////////////////////////////// Shown Dropdown Category Links Onclick ////////////////
    $(".dropdown-category").click(function(e){
        e.preventDefault();
        
        $(".header_main-group .dropdown-menu").toggleClass("dropdown-category--active");
    });

    ////////////////////////////// Shown and Slide Header Menu Onclick ////////////////
    $(".header_nav-open").click(function(e){
        e.preventDefault();
        
        $(".header_overlay").slideToggle("fast");
    });

    ////////////////////////////// Carousel Slider /////////////////////////
    // Hero Carousel Slider
    $('.hero .container .owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        autoplay:true,
        autoplayTimeout:7000,
        autoplayHoverPause:true,
        navText: ['<img src="https://img.icons8.com/external-those-icons-fill-those-icons/24/000000/external-left-arrows-those-icons-fill-those-icons-3.png"/>',
            '<img src="https://img.icons8.com/external-those-icons-fill-those-icons/24/000000/external-right-arrows-those-icons-fill-those-icons-5.png"/>' 
        ],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })
    

    ///// Main Course Carousel Slider
    $('.course .owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        lazyLoad: true,
        rewind: true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        navText: ['<img src="https://img.icons8.com/external-those-icons-fill-those-icons/24/000000/external-left-arrows-those-icons-fill-those-icons-3.png"/>',
            '<img src="https://img.icons8.com/external-those-icons-fill-those-icons/24/000000/external-right-arrows-those-icons-fill-those-icons-5.png"/>' 
        ],
        responsive:{
            0:{
                items:1
            },
            576:{
                items:2
            },
            768:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });

    ///// Main Brand Carousel Slider
    $('.brand .owl-carousel').owlCarousel({
        loop:true,
        margin:15,
        nav:false,
        navText: ['<img src="https://img.icons8.com/external-those-icons-fill-those-icons/24/000000/external-left-arrows-those-icons-fill-those-icons-3.png"/>',
            '<img src="https://img.icons8.com/external-those-icons-fill-those-icons/24/000000/external-right-arrows-those-icons-fill-those-icons-5.png"/>' 
        ],
        responsive:{
            0:{
                items:3
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });

    ////////////////////////////// Show Tabs Course /////////////////////////
    $(".new__course-tab:first-child").addClass("new__course-tab--active");
    $(".course__content").hide();
    $(".course__content:first").show();
    $(".new__course-tab").click(function(){
        // Active Tab color onclick
        $(".new__course-tab").removeClass("new__course-tab--active");
        $(this).addClass("new__course-tab--active");

        $(".course__content").hide();

        // // Get Tab Attribute
        var activeTab = $(this).attr("data-target");
        $(".course__content-"+activeTab).fadeIn();
    });
   

   /////////:::::::::::::::::   ADD POST ON THE BOOKMARK ::::::::::::::::::::://///////
    $('.addToBookmark').click(function(){

        var post_id = $(this).attr('id');

        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/add-to-bookmark/"+post_id,
            success: function(data){
                // Start Message 
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000
                })
                if ($.isEmptyObject(data.error)) {
                    Toast.fire({
                        type: 'success',
                        icon: 'success',
                        title: data.success
                    })
                }else{
                    Toast.fire({
                        type: 'error',
                        icon: 'error',
                        title: data.error
                    })
                }
            // End Message 
            } //end retrieve success data
        }); //end ajax script

    });

    $('.addToBookmarks').click(function(){

        var post_id = $(this).attr('id');

        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/add-to-bookmark/"+post_id,
            success: function(data){
                // Start Message 
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000
                })
                if ($.isEmptyObject(data.error)) {
                    Toast.fire({
                        type: 'success',
                        icon: 'success',
                        title: data.success
                    })
                }else{
                    Toast.fire({
                        type: 'error',
                        icon: 'error',
                        title: data.error
                    })
                }
            // End Message 
            } //end retrieve success data
        }); //end ajax script

    });

    

});
