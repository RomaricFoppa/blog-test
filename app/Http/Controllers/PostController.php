<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{

    public function PostDetailsView($post_id, $slug){
        $data['post'] = Post::find($post_id);
        return view('components.post_details', $data);
    }

}
