<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Comment;
use App\Models\Post;
use App\Models\CommentReply;
use Auth;

class PostComment extends Component
{
    public $comment_text;
    public $comment_reply_text;
    public $post_id = null;

    protected $listeners = ['storeComment', '$refresh'];

    public function mount($post_details_id)
    {
        $this->post_id = $post_details_id;
    }

    public function storeComment()
    {

        if(Auth::check()){

            $this->validate([
                'comment_text' => 'required|max:500'
            ]);
    
            $comment = new Comment();
            $comment->user_id = Auth::user()->id;
            $comment->post_id =  $this->post_id;
            $comment->comment_text = $this->comment_text;
            $comment->save();
    
            $this->dispatchBrowserEvent('swal:modal', [
                'position'=>'top-end',
                'title' => 'Comment created successfully',
                'icon'=>'success',
                'toast'=>true,
                'showConfirmButton' => false,
                'timer'=>3000
            ]);

            // return redirect()->to('post/details/{post_id}/{slug}');

        }else{

            $this->dispatchBrowserEvent('swal:modal', [
                'position'=>'top-end',
                'title' => 'Please login first',
                'icon'=>'warning',
                'toast'=>true,
                'showConfirmButton' => false,
                'timer'=>3000
            ]);

        }
        
        $this->comment_text = '';

    }

    public function storeCommentReply($comment_id)
    {

        if(Auth::check()){

            $this->validate([
                'comment_reply_text' => 'required|max:500',
            ],[
                'comment_reply_text.required' => 'Veuillez entrer votre commentaire svp'
            ]);

            $new_comment_reply = new CommentReply(); 
            $new_comment_reply->user_id = Auth()->user()->id;
            $new_comment_reply->post_id = $this->post_id;
            $new_comment_reply->comment_id = $comment_id;
            $new_comment_reply->comment_reply_text = $this->comment_reply_text;
            $new_comment_reply->save();

            $this->dispatchBrowserEvent('swal:modal', [
                'position' => 'top-end',
                'title' => 'Comment created successfully',
                'icon' => 'success',
                'toast' => true,
                'showConfirmButton' => false,
                'timer' => 3000
            ]);

            $this->comment_reply_text = '';

        }else {

            $this->dispatchBrowserEvent('swal:modal', [
                'position' => 'top-end',
                'title' => 'Please login first',
                'icon' => 'warning',
                'toast' => true,
                'showConfirmButton' => false,
                'timer' => 3000
            ]);

        }

    }

    /*=====================================
    DISPLAY COMMENT DATA
    ===================================== */
    public function render()
    {

        return view('livewire.post-comment', [
            'comments' => Comment::where('post_id', $this->post_id)->get(),
            'post_id' => $this->post_id
        ]);
    }
}
