<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Post;
use Storage;

class Posts extends Component
{
    use WithFileUploads;
    use WithPagination;
    use AuthorizesRequests;

    public $showModalForm = false;

    public $title;
    public $description;
    public $image;
    public $post = null;
    public $postId = null;
    public $newImage;

    protected $listeners = ['deletePost'];

    public function mount(Post $post)
    {
        $this->post = $post;
    }

    public function showCreatePostModal()
    {
        $this->showModalForm = true;
    }

    public function updatedShowModalForm()
    {
        $this->reset(); 
    }

    public function storePost()
    {

        $this->validate([
            'title' => 'required',
            'description' => 'required|max:500',
            'image' => 'required|image|max:1024|nullable',
        ]);

        $image_name = $this->image->getClientOriginalName();
        $this->image->storeAs('public/photos/', $image_name);
        $post = new Post();
        $post->title = $this->title;
        $post->slug = Str::slug($this->title);
        $post->user_id = Auth::user()->id;
        $post->description = $this->description;
        $post->image = $image_name;
        $post->save();

        $this->dispatchBrowserEvent('swal:modal', [
            'position' => 'top-end',
            'title' => 'Post created successfully',
            'icon' => 'success',
            'toast' => true,
            'showConfirmButton' => false,
            'timer' => 3000
        ]);

        $this->reset();

    }

    public function showEditPostModal($post_id)
    {
        // $this->authorize('update', $this->post);

        $this->reset();
        $this->showModalForm = true;
        $this->postId = $post_id;
        $this->loadEditForm(); 
    }

    public function loadEditForm(){

        // $this->authorize('update', $this->post);

        $post = Post::findOrFail($this->postId);
        $this->title = $post->title;
        $this->description = $post->description;
        $this->newImage = $post->image;
    }

    public function UpdatePost(){

        // $this->authorize('update', $this->post);

        $this->validate([
            'title' => 'required',
            'description' => 'required|max:500',
        ]);

        if($this->image){
            Storage::delete('public/photos/', $this->newImage);
            $this->newImage = $this->image->getClientOriginalName();
            $this->image->storeAs('public/photos/', $this->newImage);
        }

        Post::find($this->postId)->update([
            'title' => $this->title,
            'description' => $this->description,
            'slug' => Str::slug($this->title),
            'image' => $this->newImage,
        ]);

        $this->reset();

    }

    public function deleteConfirm($post_id)
    {

        // $this->authorize('delete', $this->post);

        $this->dispatchBrowserEvent('swal:confirm', [
            'title' => 'Are you sure?',
            'text' => "You won't be able to revert this!",
            'icon' => 'warning',
            'showCancelButton' => true,
            'confirmButtonColor' => '#3085d6',
            'cancelButtonColor' => '#d33',
            'confirmButtonText' => 'Yes, delete it!',
            'id' => $post_id
        ]);
    }

    public function deletePost($post_id)
    {

        // $this->authorize('delete', $this->post);
    
        $post = Post::find($post_id);
        Storage::delete('public/photos/', $post->image);
        $post->delete();

    }

    public function render(Post $post)
    {
        return view('livewire.posts', [
            'posts' => Post::orderBy('created_at', 'DESC')->paginate(5)
        ]);
    }
}
