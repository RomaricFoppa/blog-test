


<div {{ $attributes->class(['max-w-sm bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700']) }}>
    
    @isset($image)
    {{$image}}
    @endisset
   
    <div class="p-5">
        
        @isset($title)
        <h5 {{ $title->attributes->class(['mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white']) }}>
            {{$title}}
        </h5>
        @endisset
        
        @isset($body)
        <p {{ $body->attributes->class(['mb-3 font-normal text-gray-700 dark:text-gray-400']) }}>
            {{$body}}
        </p>
        @endisset
        
        <div>
            @isset($footer)
            {{$footer}}
            @endisset
        </div>
       

    </div>
</div>