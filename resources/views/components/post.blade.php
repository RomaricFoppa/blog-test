<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- ================================
    PLUGINS CSS
    ===================================-->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.2.19/tailwind.min.css" integrity="sha512-wnea99uKIC3TJF7v4eKk4Y+lMz2Mklv18+r4na2Gn1abDRPPOeef95xTzdwGD9e6zXJBteMIhZ1+68QC5byJZw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inria+Serif:ital,wght@0,400;0,700;1,300&family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <title>LaraBlog</title>
    
</head>
<body>

    <!-- ================================
    HEADER
    ===================================-->
    <header class="block px-2 bg-white border-b border-gray-200 border-solid shadow-sm header-main py-7 min-h-24">
        <nav class="nav">
            <div class="container flex flex-wrap items-center justify-between mx-auto max-w-7xl ">

                <div class="nav__logo">
                    <a href="{{ url('/') }}" class="inline-block text-3xl font-bold tracking-wide text-green-500 capitalize navbar__brand">LaraBlog</a>
                </div>

                <div class="nav__overlay">
                    <ul class="flex flex-wrap items-center nav__content ">
                        <li class="mr-4 nav__item">
                            <a href="{{ url('/') }}" class="inline-block font-sans text-base font-medium text-green-500 capitalize duration-300 cursor-pointer nav__link hover:text-green-600 active:text-green-700 ">Home</a>
                        </li>
                        <li class="mr-4 nav__item">
                            <a href="" class="inline-block font-sans text-base font-medium capitalize duration-300 cursor-pointer nav__link text-dark-500 hover:text-green-600 active:text-green-700">categories</a>
                        </li>
                        @auth
                            <li class="mr-4 nav__item">
                                <a href="{{ route('dashboard') }}" class="inline-block font-sans text-base font-medium capitalize duration-300 cursor-pointer nav__link text-dark-500 hover:text-green-600 active:text-green-700">Dashboard</a>
                            </li>
                        @else   
                            <li class="mr-4 nav__item">
                                <a href="{{ route('login') }}" class="font-sans text-base font-medium capitalize duration-300 cursor-pointer nav__link text-dark-500 hover:text-green-600 active:text-green-700">Login</a>
                            </li>
                        @endauth
                    </ul>
                </div>

            </div>
        </nav>
    </header>

    <!-- ================================
    HERO
    ===================================-->
    <div class="px-2 py-12 hero">

        <div class="container mx-auto max-w-7xl">
            <div class="grid grid-cols-1 gap-2 md:grid-cols-2">
                
                <div class="col-span-1 py-8 text-center md:text-left tex hero__text">
                    <h1 class="mb-5 font-serif text-3xl font-bold leading-tight md:text-4xl lg:text-5xl heading-primary text-dark">
                        Nous élevons l'apprentissage vers de nouveaux sommets
                    </h1>
                    <p class="w-full font-serif font-normal text-gray-600 text-md md:text-lg md:w-9/12 heading-p ">Apprentissage à domicile, à votre rythme, à <strong>l'écoute du monde</strong>. Du codage à la gestion de projet.</p>
                    <div class="w-full py-5 hero__form">
                        <form>
                            <div class="form-group">
                                <input type="text" name="" class="w-11/12 px-4 py-4 font-sans text-base font-normal leading-tight border border-gray-200 border-solid rounded-lg appearance-none focus:outline-none focus:ring focus:ring-border-400"
                                 placeholder="Search courses and topics">
                            </div>
                        </form>
                    </div>
                    <ul class="flex items-center justify-center md:justify-start hero__count">
                        <li>
                            <i class="text-green-500 fa-solid fa-clipboard-list hover:text-green-600 active:text-green-700"></i>
                            <a href="" class="mr-6 text-base font-medium leading-tight text-green-500 capitalize duration-300">2.51M Learners</a>
                        </li>
                        <li>
                            <i class="text-green-500 fa-solid fa-book hover:text-green-600 active:text-green-700"></i>
                            <a href="" class="mr-6 text-base font-medium leading-tight text-green-500 capitalize duration-300 hover:text-green-600 active:text-green-700">270 Courses</a>
                        </li>
                        <li>
                            <i class="text-green-500 fa-solid fa-user-graduate hover:text-green-600 active:text-green-700"></i>
                            <a href="hero_bg.svg" class="mr-6 text-base font-medium leading-tight text-green-500 capitalize duration-300 hover:text-green-600 active:text-green-700">116 Institutions</a>
                        </li>
                    </ul>
                </div>
                <!-- /.hero__text -->
                
                <div class="flex justify-center col-span-1 hero__img" >
                    <div class="w-3/4">
                        <img src="{{ asset('frontend/assets/images/hero_bg.svg') }}" alt="" class="w-full">
                    </div>
                </div>
                <!-- /.hero__img -->
            </div>
          
        </div>

    </div>
    <!-- /.hero -->

    <!-- ================================
    MAIN
    ===================================-->
    <main class="px-2">

        <!-- /.widgets -->
        <section class="py-10 bg-green-200 widgets">
            <div class="container flex justify-center mx-auto max-w-7xl" >
                <div class="grid grid-cols-1 lg:grid-cols-4 md:grid-cols-2 sm:grid-col-2">
                    <div class="col-span-1 mb-2">
                        <span class="font-sans text-base font-medium capitalize text-dark-400" style="font-family:Roboto,sans-serif;">Apprentissage actif par projet</span>
                    </div>
                    <div class="col-span-1 mb-2">
                        <span class="font-sans text-base font-medium capitalize text-dark-400" style="font-family:Roboto,sans-serif;">Acquérir de vraies compétences employables</span>
                    </div>
                    <div class="col-span-1 mb-2">
                        <span class="font-sans text-base font-medium capitalize text-dark-400" style="font-family:Roboto,sans-serif;">Apprenez selon votre emploi du temps</span>
                    </div>
                    <div class="col-span-1 mb-2">
                        <span class="font-sans text-base font-medium capitalize text-dark-400" style="font-family:Roboto,sans-serif;">Rencontrez des apprenants du monde entier</span>
                    </div>
                </div>
            </div>
        </section>

        <!-- /.course -->
        <section class="course bg-cyan-300">
            <div class="container mx-auto max-w-7xl ">
                <div class="w-full py-10 text-center course__title">
                    <h1 class="font-serif text-xl font-bold md:text-3xl lg:text-4xl leading-16 text-dark-500" style="font-family:Inria,serif;">Des centaines de cours en ligne qui ont fait évoluer de nombreuses carrières dans le monde entier</h1>
                    <p class="p-5 font-sans text-base font-medium text-gray-600 capitalize" style="font-family:Roboto,sans-serif;">Explorez nos écoles pour trouver votre programme idéal</p>
                </div>
                <div class="flex justify-center course__content">

                    <div class="grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">

                        @php
                            $posts = App\Models\Post::latest()->get();
                        @endphp

                        @foreach ($posts as $post)

                        <!-- /.col -->
                        <div class="col-span-1">
                            <!-- /.card 1 -->
                            <div class="card">
                                <a href="{{ url('post/details/'.$post->id.'/'.$post->slug) }}">
                                    <div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
                                        <a href="{{ url('post/details/'.$post->id.'/'.$post->slug) }}">
                                            <img class="rounded-t-lg" src="{{ asset('storage/photos/'.$post->image) }}" alt="" />
                                        </a>
                                        <div class="p-5">
                                            <a href="{{ url('post/details/'.$post->id.'/'.$post->slug) }}">
                                                <h5 class="mb-2 text-xl font-bold tracking-tight text-gray-900 dark:text-white">{{ $post->title }}</h5>
                                            </a>
                                            <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">{{ Str::limit($post->description, 90) }}</p>
                                            <a href="{{ url('post/details/'.$post->id.'/'.$post->slug) }}" class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-green-700 rounded-lg hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">
                                                Read more
                                                <svg class="w-4 h-4 ml-2 -mr-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                                            </a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <!-- /.card 1 -->
                        </div>
                        <!-- /.col -->

                        @endforeach

                    </div>
                </div>
            </div>
        </section>

    </main>
    <!-- /.main -->
    

</body>
</html>