<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>LaraBlog | Post-details</title>
    <meta name="author" content="name" />
    <meta name="description" content="description here" />
    <meta name="keywords" content="keywords,here" />
    <link rel="stylesheet" href="https://unpkg.com/tailwindcss@2.2.19/dist/tailwind.min.css"/>
    <!--Replace with your tailwind.css once created-->

	@livewireStyles
  </head>

<body class="bg-gray-100 font-sans leading-normal tracking-normal">

    <!-- ================================
    HEADER
    ===================================-->
    <header class="header-main py-7 min-h-24 block bg-white border-b border-gray-200 border-solid shadow-sm">
        <nav class="nav">
            <div class="container flex flex-wrap items-center max-w-7xl justify-between mx-auto ">

                <div class="nav__logo">
                    <a href="{{ url('/') }}" class="navbar__brand text-green-500 text-3xl font-bold inline-block capitalize tracking-wide">LaraBlog</a>
                </div>

                <div class="nav__overlay">
                    <ul class="nav__content flex flex-wrap items-center  ">
                        <li class="nav__item mr-4">
                            <a href="{{ url('/') }}" class="nav__link capitalize duration-300 inline-block font-medium text-base font-sans text-green-500 hover:text-green-600 active:text-green-700 cursor-pointer ">Home</a>
                        </li>
                        <li class="nav__item  mr-4">
                            <a href="" class="nav__link capitalize duration-300 inline-block font-medium text-base font-sans text-dark-500 hover:text-green-600 active:text-green-700 cursor-pointer">categories</a>
                        </li>
                        @auth
                            <li class="nav__item  mr-4">
                                <a href="{{ route('dashboard') }}" class="nav__link capitalize duration-300 inline-block  font-medium text-base font-sans text-dark-500 hover:text-green-600 active:text-green-700 cursor-pointer">Dashboard</a>
                            </li>
                        @else   
                            <li class="nav__item  mr-4">
                                <a href="{{ route('login') }}" class="nav__link capitalize duration-300 font-medium text-base font-sans text-dark-500 hover:text-green-600 active:text-green-700 cursor-pointer">Login</a>
                            </li>
                        @endauth
                    </ul>
                </div>

            </div>
        </nav>
    </header>

	<!--Container-->
	<div class="container w-full md:max-w-3xl mx-auto pt-20">

		<div class="w-full px-4 md:px-6 text-xl text-gray-800 leading-normal" style="font-family:Georgia,serif;">

			<!--Title-->
			<div class="font-sans">
				<p class="text-base md:text-sm text-green-500 font-bold">&lt; <a href="{{ url('/') }}" class="text-base md:text-sm text-green-500 font-bold no-underline hover:underline">RETOUR AU BLOG</a></p>
                    <h1 class="font-bold font-sans break-normal text-gray-900 pt-6 pb-2 text-3xl md:text-4xl">{{ $post->title }}</h1>
                    <p class="text-sm md:text-base font-normal text-gray-600">Published {{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</p>
			</div>


			<!--Post Content-->

            <!-- Post Content Image Layer -->
            <div class="mt-3">
                <img src="{{ asset('storage/photos/'.$post->image) }}" alt="">
            </div>

			<!--Lead Para-->
			<p class="py-6">
                {{ $post->description }}
			</p>

			{{-- <blockquote class="border-l-4 border-green-500 italic my-8 pl-8 md:pl-12">Example of blockquote - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula.</blockquote> --}}

			<!--/ Post Content-->

		</div>

		<!--Divider-->
		<hr class="border-b-1 border-gray-400 mb-5 mx-4">

        <!-- Blog Comment Section -->
        {{-- @livewire('post-comment') --}}
		<div class="post_form">
			@livewire('post-comment', ['post_details_id' => $post->id])
		</div>

		<!--Divider-->
		<hr class="border-b-1 border-gray-400 mb-8 mx-4">

	</div>
	<!--/container-->

	@livewireScripts
</body>

</html>
