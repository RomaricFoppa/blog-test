@php
    $classes = "text-white text-sm rounded-lg bg-yellow-400 duration-300 hover:bg-yellow-500 px-5 py-2.5 font-medium focus-ring-4 focus:ring-yellow-300 focus:outline-none shadow-md";
    $type = "button";
@endphp


<button {{ $attributes->merge(['class' => $classes, 'type' => $type]) }}>
    {{ $slot }}
</button>