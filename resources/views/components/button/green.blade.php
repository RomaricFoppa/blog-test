@php
    $classes = "text-white text-sm rounded-lg bg-green-700 border border-transparent duration-300 hover:bg-green-800 py-2.5 px-5 font-medium focus-ring-4 focus:ring-green-300 focus:outline-none shadow-md";
    $type = "button";
@endphp


<button {{ $attributes->merge(['class' => $classes, 'type' => $type]) }}>
    {{ $slot }}
</button>