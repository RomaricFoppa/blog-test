@php
    $classes = "text-white text-sm rounded-lg bg-red-700 duration-300 hover:bg-red-800 px-5 py-2.5 font-medium focus-ring-4 focus:ring-red-300 focus:outline-none shadow-md dark:focus:ring-yellow-900";
    $type = "button";
@endphp


<button {{ $attributes->merge(['class' => $classes, 'type' => $type]) }}>
    {{ $slot }}
</button>