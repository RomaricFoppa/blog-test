@php
    $classes = "text-white text-sm rounded-lg bg-blue-700 duration-300 hover:bg-blue-800 py-2.5 px-5 font-medium focus-ring-4 focus:ring-blue-300 focus:outline-none shadow-md";
    $type = "button";
@endphp


<button {{ $attributes->merge(['class' => $classes, 'type' => $type]) }}>
    {{ $slot }}
</button>