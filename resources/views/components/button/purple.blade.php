@php
    $classes = "text-gray-900 text-sm rounded-lg bg-white border border-gray-300 duration-300 hover:bg-gray-100 py-2.5 px-5 font-medium focus-ring-4 focus:ring-gar-300 focus:outline-none shadow-md";
    $type = "button";
@endphp


<button {{ $attributes->merge(['class' => $classes, 'type' => $type]) }}>
    {{ $slot }}
</button>