@php
    $classes = "text-white text-sm rounded-lg bg-gray-800 duration-300 hover:bg-gray-900 px-5 py-2.5  focus-ring-4 focus:ring-gray-300 focus:outline-none shadow-md
    dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-700 dark:border-gray-700";
    $type = "button";
@endphp


<button {{ $attributes->merge(['class' => $classes, 'type' => $type]) }}>
    {{ $slot }}
</button>