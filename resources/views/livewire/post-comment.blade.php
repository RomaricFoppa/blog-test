<div>
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>

    <div class="w-full flex flex-col mx-4">

        @if(session()->has('message'))
            <div class="bg-green-100 border border-green-500 py-3 px-4 text-green-700 font-medium rounded">
                <strong>{{ $message }}</strong>
            </div>
        @endif

        <!--Author-->
        @foreach ($comments as $comment)

            <div class="flex w-full items-center font-sans px-4 py-4">
            
                <img class="w-10 h-10 rounded-full mr-4" src="{{ empty($comment['user']['profile_photo_path']) ? url('frontend/assets/images/avatar-12.png') : url('storage/photos/'.$comment->user->profile_photo_path) }}" alt="Avatar of Author">

                <div class="flex-1 px-2" x-data="{
                            tabComment: $refs.tabComment.getBoundingClientRect().height,
                            tabCommentHeight: $refs.tabCommentHeight.getBoundingClientRect().height
                        }">

                    <p class="text-base font-bold text-base md:text-xl leading-none mb-1">
                        {{ $comment['user']['name'] }}
                    </p>

                    <p class="text-gray-600 text-sm md:text-base">{{ $comment->comment_text }} </p>

                    <x-jet-button  x-on:click="tabComment === 0? tabComment = tabCommentHeight : tabComment = 0;" class="tab__comment-button">Reply
                    </x-jet-button>

                    <!-- Comment Reply Form -->
                    <div x-ref="tabComment" :style="{ height: tabComment+'px' }" class="transition-all h-0 duration-500 overflow-hidden tab__comment-form mt-3">
                        <div x-ref="tabCommentHeight" class="tab__comment-form">
                            <form>
                                <textarea rows="3" wire:model.lazy="comment_reply_text" class="shadow-sm focus:ring-indigo-500 appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"></textarea>
                            </form>
                            <x-jet-button class="mt-2 bg-green-500 hover:bg-green-600" wire:click="storeCommentReply({{ $comment->id }})">Save</x-jet-button>
                        </div>
                    </div>
                    <!-- /.Comment Reply Form -->

                </div>
            
            </div>

            <!-- /.Comment Reply Render -->
            <div class="comment_reply">
                @php
                    $comment_replies = App\Models\CommentReply::where('post_id', $post_id)->where('comment_id', $comment->id)->get();    
                @endphp
                
                @foreach ($comment_replies as $comment)
                    <div class="flex w-full items-center font-sans px-4 ml-8 pb-3">
                        <img class="w-10 h-10 rounded-full mr-4" src="{{ empty($comment['user']['profile_photo_path']) ? url('frontend/assets/images/avatar-13.png') : url('storage/photos/'.$comment->user->profile_photo_path) }}" alt="Avatar of Author">
                        <div class="flex-1 px-2">
                            <p class="text-base font-bold text-base md:text-xl leading-none mb-1">
                                {{ $comment['user']['name'] }}
                            </p>
                            <p class="text-gray-600 text-sm md:text-base">{{ $comment->comment_reply_text }} </p>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- /.Comment Reply Render -->

        @endforeach
		<!--/Author-->

        <!--Commment Form-->
        <div class="flex flex-col justify-start md:justify-start my-auto md:pt-0 pb-4">
            <div class="box space-y-7 w-full  w-1/2 mt-5">
                <form>
                    <div class="sm:col-span-6">
                        <label for="comment_text" class="block text-xl font-medium text-gray-700">Add Comment</label>
                        <div class="mt-2">
                        <textarea id="comment_text" rows="3" wire:model.lazy="comment_text" class="shadow-sm focus:ring-indigo-500 appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"></textarea>
                            @if ($errors->has('comment_text'))
                                <span class="text-red-500 font-medium">
                                    <strong>{{ $errors->first('comment_text') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </form>
                <!-- ./form -->
            </div>
            <!-- ./box -->
            <div class="mt-2">
                <x-jet-button class="bg-green-500 hover:bg-green-600" wire:click="$emit('storeComment')">Save</x-jet-button>
            </div>
        </div>
    
    </div>


<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
    /*=============================
    POST DATA ALERT
    =============================*/
    window.addEventListener('swal:modal',function(event){ 
        Swal.fire(event.detail);
    });

    /*=============================
    REPLY TO A COMMENT 
    =============================*/
    const tabCommentButtons = document.querySelectorAll('.tab__comment-button');
    const tabCommentForms = document.querySelectorAll('.tab__comment-form');
    
    // tabCommentForms.forEach((tabCommentForm) => {
    //     tabCommentForm.classList.add('hidden');
    // });
    
    // if(tabCommentButtons){
    //     tabCommentButtons.forEach((tabCommentButton) => {
    //         tabCommentButton.addEventListener('click', event => {
    //             const getAttributeData = event.currentTarget.getAttribute('data-tab');
    //             tabCommentForms.forEach((tabCommentForm) => {
    //                 tabCommentForm.classList.add('hidden');
    //             });
    //             document.querySelector('.tab__comment-form--'+`${getAttributeData}`).classList.remove('hidden');
    //             document.querySelector('.tab__comment-form--'+`${getAttributeData}`).classList.add('block');
    //         });
    //     });
    // }

</script>

</div>
