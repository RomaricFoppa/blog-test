<div class="container mx-auto mt-2">
  <x-jet-banner></x-jet-banner>
    <div class="flex content-center m-2 p-2">
        <x-jet-button class="bg-red-300 " wire:click="showCreatePostModal">Create Post {{$post}}</x-jet-button>
    </div>
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
          <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table class="w-full divide-y divide-gray-200">
              <thead class="bg-gray-50 dark:bg-gray-600 dark:text-gray-200">
                <tr>
                  <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">Id</th>
                  <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">Title</th>
                  <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">Description</th>
                  <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">Status</th>
                  <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">Image</th>
                  <th scope="col" class="relative px-6 py-3">Action</th>
                </tr>
              </thead>
              <tbody class="bg-white divide-y divide-gray-200">
                <tr></tr>
                  @foreach ($posts as $key => $post)
                    @can('viewAny', $post)
                      <tr>
                        <td class="px-6 py-4 whitespace-nowrap" >{{ $key+1 }}</td>
                        <td class="px-6 py-4 whitespace-nowrap" >{{ $post->title }}</td>
                        <td class="px-6 py-4 whitespace-nowrap" >{{ $post->description }}</td>
                        <td class="px-6 py-4 whitespace-nowrap">Active</td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <img src="{{ asset('storage/photos/'.$post->image) }}" />
                        </td>
                        <td class="px-6 py-4 text-right text-sm" style="width: 20%;">

                          {{-- Only user post can update this post --}}
                          @can('update', $post)
                            <x-jet-button wire:click="showEditPostModal({{ $post->id }})">Edit</x-jet-button>
                          @endcan

                          {{-- Only user post can delete this post --}}
                          @can('delete', $post)
                            <x-jet-button class="bg-green-700 hover:bg-green-800 focus:ring-2 focus:ring-green-200" wire:click="deleteConfirm({{ $post->id }})">Delete</x-jet-button>
                          @endcan

                        </td>
                      </tr>
                    @endcan
                  @endforeach
                <!-- More items... -->
              </tbody>
            </table>
            <div class="m-2 p-2">{{ $posts->links() }}</div>
          </div>
        </div>
    </div>

    <!-- Modal -->
    <x-jet-dialog-modal wire:model="showModalForm">
        <x-slot name="title">Create Post</x-slot>
        <x-slot name="content" class="max-h-50 overflow-y-auto">
            <div class="space-y-8 divide-y divide-gray-200 mt-10">
                <form enctype="multipart/form-data">
                  <div class="sm:col-span-6">
                    <label for="title" class="block text-sm font-medium text-gray-700"> Post Title </label>
                    <div class="mt-2">
                        <input type="text" id="title" wire:model.lazy="title" name="title" class="block w-full transition duration-150 ease-in-out appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                        @if ($errors->has('title'))
                            <span class="text-red-500 font-medium">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                  </div>
                  <div class="sm:col-span-6">
                        <div class="w-full p-2 mt-2">
                            @if ($newImage)
                                Post Image:
                                <img src="{{ asset('storage/photos/'.$newImage) }}" alt="">
                            @endif
                            @if ($image)
                              Photo Preview:
                              <img src="{{ $image->temporaryUrl() }}" alt="">
                             @endif
                        </div>
                  </div>
                  <!-- LOADER FOR IMAGE -->
                  <div class="sm:col-span-6">
                    <div class="loader_img" wire:loading wire:target="image">
                      <img src="{{ asset('loader/loading-buffering.gif') }}" alt="">
                    </div>
                  </div>
                  <div class="sm:col-span-6">
                    <label for="title" class="block text-sm font-medium text-gray-700"> Post Image </label>
                    <div class="mt-2">
                        <input type="file" id="image" wire:model="image" name="image" class="block w-full transition duration-150 ease-in-out appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                        @if ($errors->has('image'))
                            <span class="text-red-500 font-medium">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                  </div>
                  <div class="sm:col-span-6 pt-5">
                    <label for="description" class="block text-sm font-medium text-gray-700">Description</label>
                    <div class="mt-2">
                      <textarea id="description" rows="3" wire:model.lazy="description" class="shadow-sm focus:ring-indigo-500 appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"></textarea>
                        @if ($errors->has('description'))
                            <span class="text-red-500 font-medium">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                  </div>
                </form>
              </div>
        </x-slot>
        <x-slot name="footer">
            @if ($postId)
            <x-jet-button wire:click="UpdatePost">Update</x-jet-button>
            @else 
            <x-jet-button wire:click="storePost">Store</x-jet-button>
            @endif
            
        </x-slot>
    </x-jet-dialog-modal>

</div>


<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
  /*=============================
  POST DATA ALERT
  =============================*/
  window.addEventListener('swal:modal',function(event){ 
      Swal.fire(event.detail);
  });

  /*=============================
  DELETE DATA ALERT
  =============================*/
  window.addEventListener('swal:confirm',function(event) {
    Swal.fire(event.detail)
    .then((result) => {
      if(result.isConfirmed){
        Livewire.emit('deletePost', event.detail.id);
        Swal.fire(
          'Deleted!',
          'Post deleted successfully',
          'success'
        )
      }
    });
  }); //End of confirm alert

</script>
